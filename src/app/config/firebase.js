import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/database';
import 'firebase/auth';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyDauykcEgKmw-JuM5lqq2EbIO0Xn-Avb4U", 
    authDomain: "socianets-87602.firebaseapp.com",
    databaseURL: "https://socianets-87602.firebaseio.com",
    projectId: "socianets-87602",
    storageBucket: "socianets-87602.appspot.com",
    messagingSenderId: "455003468871",
    appId: "1:455003468871:web:7e2b48e3f8ec7262266d62",
    measurementId: "G-5Z4NSGQJJ3"

}


firebase.initializeApp(firebaseConfig);
firebase.firestore();

export default firebase;