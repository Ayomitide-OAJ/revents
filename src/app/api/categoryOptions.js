export const categoryData = [
    {key: 'drinks', text: 'Drinks', value: 'drinks'},
    {key: 'word', text: 'Word & Spirit', value: 'word'},
    {key: 'film', text: 'Film', value: 'film'},
    {key: 'food', text: 'Food', value: 'food'},
    {key: 'music', text: 'Music', value: 'music'},
    {key: 'travel', text: 'Travel', value: 'travel'},
];